import re

from utils import clean_the_shit_out, make_the_string_a_timedelta, convert_string_to_kilobytes_per_second

avg, stdev, max = 'Avg', 'Stdev', 'Max'
reqs_sec = 'Reqs/sec'
latency = 'Latency'
latency_dist = 'Latency Distribution'
latency_dist_percentile = ['50%', '75%', '90%', '99%']
http_codes = 'HTTP codes'
throughput = 'throughput'
ram = 'ram'
cpu = 'cpu'
disk = 'disk'


def curate_data(data_path):
    exclusion_regex = r"(?:\d+ \/ \d+ \[[-+|=*|>]+\] +\d+.\d+%[ *\d(s|m)]*)|(?:Done!)"

    vanilla_result_dump = None

    vrt = open(data_path)
    lines = vrt.read()
    matches = re.sub(exclusion_regex, '', lines)
    lines = matches.splitlines()
    lines = clean_the_shit_out(lines)
    lines = [l.strip() for l in lines]

    tests = {}
    connections = None
    resource = None

    for line in lines:
        pieces = clean_the_shit_out(line.split(' '))

        # Beginning of the test
        first_word = pieces[0]
        if first_word == 'Bombarding':
            connections = pieces[len(pieces) - 2]
            url = pieces[1].split('/')
            resource = url[3]
            if resource not in tests:
                tests[resource] = {}
            tests[resource][connections] = {reqs_sec: {avg: None, stdev: None, max: None},
                                            latency: {avg: None, stdev: None, max: None},
                                            latency_dist: {p: None for p in latency_dist_percentile},
                                            http_codes: {},
                                            throughput: None}
            continue

        # Reqs/sec
        if first_word == reqs_sec:
            tests[resource][connections][reqs_sec] = {avg: float(pieces[1]),
                                                      stdev: float(pieces[2]),
                                                      max: float(pieces[3])}
            continue

        # Latency
        if ' '.join(pieces) != latency_dist and first_word == latency:
            tests[resource][connections][latency] = {avg: make_the_string_a_timedelta(pieces[1]),
                                                     stdev: make_the_string_a_timedelta(pieces[2]),
                                                     max: make_the_string_a_timedelta(pieces[3])}
            continue

        # Latency distribution
        if first_word in latency_dist_percentile:
            tests[resource][connections][latency_dist][first_word] = make_the_string_a_timedelta(pieces[1])
            continue

        # HTTP errors
        if first_word == '1xx':
            errors = [p.rstrip(',') for p in pieces]
            tests[resource][connections][http_codes] = {errors[i]: int(errors[i + 1]) for i in range(0, len(errors), 2)}
            continue

        if first_word == 'others':
            tests[resource][connections][http_codes][pieces[0]] = int(pieces[1])
            continue

        # Throughput
        if first_word == 'Throughput:':
            tests[resource][connections][throughput] = convert_string_to_kilobytes_per_second(pieces[1])
            continue

    return tests
