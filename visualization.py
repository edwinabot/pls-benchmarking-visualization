import matplotlib.pyplot as plt

import data_curation as dc

connections = ['10', '100', '1000']

width = .2

colors = ['#DCE8BA', '#F3D179', '#F09872', '#F46060', ]
error_colors = ['#DC552C', '#404969', '#BDE4F4', '#F5FEFF', ]

def visualize_reqs_per_second(endpoint, files, png_path=None):
    if png_path:
        plt.figure(figsize=(10, 8), dpi=120)

    curated_data = []
    data_labels = []
    for f in files[:4]:
        curated_data.append(dc.curate_data(f))
        data_labels.append(f.split('/')[-1].split('_')[0].capitalize())

    xs = [i for i, _ in enumerate(connections)]
    plt.title(f'[/{endpoint}] average {dc.reqs_sec}')
    plt.ylabel(dc.reqs_sec)
    plt.xticks([i + (width / 2) for i, _ in enumerate(connections)], connections, horizontalalignment='center')
    plt.xlabel('Concurrent connections')

    for i, f in enumerate(files[:4]):
        test = curated_data[i]
        data = [test[endpoint][c][dc.reqs_sec][dc.avg] for c in connections]
        bc = plt.bar([x + (0.25 * i) for x in xs],
                     data,
                     width, color=colors[i],
                     label=data_labels[i], zorder=2)

        for i, r in enumerate(bc.patches):
            plt.text(r.xy[0] + (width / 2), data[i] + 3, str(data[i]), ha='center', va='bottom')

    lgnd_ax1 = plt.legend(bbox_to_anchor=(0, -.2), loc=3, borderaxespad=0., title='Reqs/sec')
    ax2 = plt.twinx()
    ax2.set_ylim(bottom=0, top=500)
    for i, f in enumerate(files[:4]):
        test = curated_data[i]
        data_errs = [test[endpoint][c][dc.http_codes]['2xx'] for c in connections]
        error_points = [10000 - d for d in data_errs]
        ax2.scatter([x + (0.25 * i) for x in xs], error_points, label=data_labels[i], color=error_colors[i])
    ax2.set_ylabel('Failed requests')
    lgnd_ax2 = ax2.legend(bbox_to_anchor=(1, -.2), loc=4, borderaxespad=0., title='Failed requests')


    if png_path:
        png_path.rstrip('/')
        plt.savefig(f'{png_path}/{endpoint}_reqs_sec.png', dpi=300, format='png', bbox_extra_artists=(lgnd_ax1, lgnd_ax2), bbox_inches='tight')
    else:
        plt.show()


def visualize_latency(endpoint, files, png_path=None):
    if png_path:
        plt.figure(figsize=(10, 8), dpi=120)

    curated_data = []
    data_labels = []
    for f in files[:4]:
        curated_data.append(dc.curate_data(f))
        data_labels.append(f.split('/')[-1].split('_')[0].capitalize())

    xs = [i for i, _ in enumerate(connections)]
    plt.title(f'[/{endpoint}] average {dc.latency}')
    plt.xticks([i for i, _ in enumerate(connections)], connections)
    plt.xlabel('Concurrent connections')
    plt.ylabel(' '.join([dc.latency.capitalize(), '(ms)']))

    for i, f in enumerate(files[:4]):
        test = curated_data[i]
        data = [test[endpoint][c][dc.latency][dc.avg].total_seconds() * 1000 for c in connections]
        bc = plt.bar([x + (0.25 * i) for x in xs],
                     data,
                     width, color=colors[i],
                     label=data_labels[i], zorder=2)

        for i, r in enumerate(bc.patches):
            plt.text(r.xy[0] + (width / 2), data[i] + 3, '{:.2f}'.format(data[i]), ha='center', va='bottom')

    lgnd_ax1 = plt.legend(bbox_to_anchor=(0, -.2), loc=3, borderaxespad=0., title='Latency')
    ax2 = plt.twinx()
    ax2.set_ylim(bottom=0, top=500)
    for i, f in enumerate(files[:4]):
        test = curated_data[i]
        data_errs = [test[endpoint][c][dc.http_codes]['2xx'] for c in connections]
        error_points = [10000 - d for d in data_errs]
        ax2.scatter([x + (0.25 * i) for x in xs], error_points, label=data_labels[i], color=error_colors[i])
    ax2.set_ylabel('Failed requests')
    lgnd_ax2 = ax2.legend(bbox_to_anchor=(1, -.2), loc=4, borderaxespad=0., title='Failed requests')

    if png_path:
        png_path.rstrip('/')
        plt.savefig(f'{png_path}/{endpoint}_latency.png', dpi=300, format='png', bbox_extra_artists=(lgnd_ax1, lgnd_ax2), bbox_inches='tight')
    else:
        plt.show()
