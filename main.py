from visualization import visualize_latency, visualize_reqs_per_second
import data_curation as dc
import argparse

parser = argparse.ArgumentParser(description='Visualize some benchmarks')

endpoit_arguments = {dc.throughput: dc.throughput, dc.ram: dc.ram, dc.cpu: dc.cpu, dc.disk: dc.disk}
metrics_arguments = {dc.latency: dc.latency, dc.reqs_sec: dc.reqs_sec}

metrics = parser.add_mutually_exclusive_group()
for a in metrics_arguments:
    metrics.add_argument(f'--{a}', action='store_true')

endpoints = parser.add_mutually_exclusive_group()
for b in endpoit_arguments:
    endpoints.add_argument(f'--{b}', action='store_true')

parser.add_argument('--png-path', action='store')

parser.add_argument('--files', action='append')

if __name__ == '__main__':
    args = vars(parser.parse_args())
    endpoint = [endpoit_arguments[key] if key in endpoit_arguments and value else None for key, value in args.items()]
    metric = [metrics_arguments[key] if key in metrics_arguments and value else None for key, value in args.items()]
    endpoint = next(filter(lambda x: x is not None, endpoint))
    metric = next(filter(lambda x: x is not None, metric))
    files = args['files']
    path = args['png_path']
    if metric == dc.reqs_sec:
        plt = visualize_reqs_per_second(endpoint, files, png_path=path)
    elif metric == dc.latency:
        plt = visualize_latency(endpoint, files, png_path=path)
    else:
        raise Exception('You are asking for too much magic')
