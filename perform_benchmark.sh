#!/usr/bin/env bash

cd ~/go/bin/


./bombardier --connections=10 --requests=10000 --timeout=10s --latencies http://localhost:8000/throughput
sleep 3
./bombardier --connections=100 --requests=10000 --timeout=10s --latencies http://localhost:8000/throughput
sleep 6
./bombardier --connections=1000 --requests=10000 --timeout=10s --latencies http://localhost:8000/throughput
sleep 9

./bombardier --connections=10 --requests=10000 --timeout=10s --latencies http://localhost:8000/ram
sleep 3
./bombardier --connections=100 --requests=10000 --timeout=10s --latencies http://localhost:8000/ram
sleep 6
./bombardier --connections=1000 --requests=10000 --timeout=10s --latencies http://localhost:8000/ram
sleep 9

./bombardier --connections=10 --requests=10000 --timeout=10s --latencies http://localhost:8000/cpu
sleep 3
./bombardier --connections=100 --requests=10000 --timeout=10s --latencies http://localhost:8000/cpu
sleep 6
./bombardier --connections=1000 --requests=10000 --timeout=10s --latencies http://localhost:8000/cpu
sleep 9

./bombardier --connections=10 --requests=10000 --timeout=10s --latencies http://localhost:8000/disk
sleep 3
./bombardier --connections=100 --requests=10000 --timeout=10s --latencies http://localhost:8000/disk
sleep 6
./bombardier --connections=1000 --requests=10000 --timeout=10s --latencies http://localhost:8000/disk
