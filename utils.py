from datetime import timedelta


def clean_the_shit_out(strings):
    """
    Removes strings which only contains whitespace or line breaks
    :param lines:
    :return:
    """
    return list(filter(lambda s: s if s not in ['\n', ' ', '-'] else None, strings))


def make_the_string_a_timedelta(value):
    if value[-2:] == 'ms':
        value = timedelta(milliseconds=float(value[:-2]))
    elif value[-1:] == 's':
        value = timedelta(seconds=float(value[:-1]))
    else:
        raise ValueError('Invalid time unit')

    return value


def convert_string_to_kilobytes_per_second(value):
    if value[-4:] == 'MB/s':
        return float(value[:-4]) * 1024
    return float(value[:-4])
